import java.util.*;

public class App {
    private static char[][] table = { { '-', '-', '-' }, { '-', '-', '-' }, { '-', '-', '-' } };
    private static int row;
    private static int col;
    private static Scanner kb = new Scanner(System.in);
    private static char turn = 'o';
    private static int count = 0 ;

    public static void main(String[] args) throws Exception {
        while (true) {
            printWelcome();
            tableOX();
            playerTurn();
            inputRowCol();
            if (isFinish()) {
                showresult();
                break;
            }
            switchTurn();
            
        }
        tableOX();

    }
    private static void printWelcome() {
        System.out.println("Welcome OX ");
    }

    private static void tableOX() {
        for (int r = 0; r < 3; r++) {
            for (int c = 0; c < 3; c++) {
                System.out.print(table[r][c] + "  ");
            }
            System.out.println();
        }
    }

    private static void playerTurn() {
        System.out.println("turn " + turn);
    }

    private static void inputRowCol() {
        System.out.print("Please input row,col : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        System.out.println(row + " " + col);
        table[row][col] = turn;
        count++;

    }

    private static void switchTurn() {
        if (turn == 'x') {
            turn = 'o';
        } else {
            turn = 'x';
        }
    }

    private static boolean isFinish() {
        if (checkWin()) {
            return true;
        }
        if (checkDraw()) {
            return true;
        }
        return false;
    }

    private static boolean checkWin() {
        if (checkRow()) {
            return true;
        }
        if (checkCol()) {
            return true;
        }
        if (checkX()) {
            return true;
        }
        return false;
    }

    private static boolean checkRow() {
        return false;
    }

    private static boolean checkCol() {
        return false;
    }

    private static boolean checkX() {
        return false;
    }

    private static boolean checkDraw() {
        if(count==9){
            return true;
        }
        return false;
    }
    private static void showresult() {
        if(checkDraw()){
            System.out.println("Draw!!!");
        }
    }

}
